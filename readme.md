# how to run

## prerequisites

install coreutils on mac

```
brew install coreutils
```

if run with java, need to have java installed


## testing data
* download testing data from [past contest](https://cemc.uwaterloo.ca/contests/past_contests.html)
* put it under contests/[year], usally the name is `all_data.zip`
* unzip it in current folder


## source code
* all source code need to be under their corresponding folder

    * `contests/[year]/[junior/senior]/[file name].[py/java]`

* file name need to be just j1/j2 or s1/s2 etc.

##  run the test
run the script in project's root dir, and it will execute all the test cases in the testing data

Note: there's case source file name is different from folder name in test data, testing will fail, then need to update the folder name of test data

**python code**

```
    ./scripts/run_py.py [relative_path_to_file]
```

E.G.

```
    ./scripts/run_py.sh contests/2021/junior/j5.py
```

**java code**

```
    ./scripts/run_java.py [relative_path_to_file]
```

E.G.

```
    ./scripts/run_java.sh contests/2021/junior/j5.java
```

