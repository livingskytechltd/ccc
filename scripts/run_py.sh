test_file=$2
if [ "$test_file" != "" ]; then
	./scripts/run_single.sh python3 $1 $2
else
	./scripts/run.sh $1 python3
fi