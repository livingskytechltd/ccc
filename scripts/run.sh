

file_path=$1
cmd=$2
test_file=$3


if [ ! -f "$file_path" ]; then
    echo "${red}# invalid file path: '$file_path'${reset}"
		exit 1
fi

IFS='/' read -r -a path_part <<< "$file_path"
echo ''
echo '# =========================================='
echo '# running file: '$file_path
echo '# =========================================='
echo '#'

# param: contests/2021/junior/j5.py

data_prefix=${path_part[0]}/${path_part[1]}
file_name=${path_part[3]%.*}

data_folder=./$data_prefix/all_data/${path_part[2]}_data/$file_name
if [ ! -d "$data_folder" ]; then
  data_folder=./$data_prefix/all_data/${path_part[2]}/$file_name
fi
data_folder=$(echo $data_folder | tr '[:upper:]' '[:lower:]');

if [[ "$OSTYPE" == "darwin"* ]]; then
	alias date='gdate'
fi



for input in $data_folder/*.in; do
		./scripts/run_single.sh $cmd $file_path $input
done

echo '# DONE '
echo '# =========================================='
echo ''