red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

cmd=$1
file_path=$2
input=$3

if [ ! -f "$input" ]; then
	echo "${red}# invalid test input file: '$input'${reset}"
	exit 1
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
	alias date='gdate'
fi

output=${input%.*}.out.me
echo '# input: '$input
start=`date +%s%3N`
$cmd $file_path < $input > $output
end=`date +%s%3N`

runtime="$((end-start))ms"

# compare output with answer
diff --brief $output ${input%.*}.out >/dev/null
comp_value=$?
if [ $comp_value -eq 1 ]
then
		echo "# ${red}FAILED, $runtime${reset}"
else
		echo "# ${green}PASS, $runtime${reset}"
fi